%?python_enable_dependency_generator
%define srcname nmstate
%define libname libnmstate

Name:           nmstate
Version:        0.2.7
Release:        1
Summary:        Declarative network manager API
License:        LGPLv2+
URL:            https://github.com/%{srcname}/%{srcname}
Source0:        %{url}/releases/download/v%{version}/%{srcname}-%{version}.tar.gz
Source1:        %{url}/releases/download/v%{version}/%{srcname}-%{version}.tar.gz.asc
Source2:        gpgkey-F7910D93CA83D77348595C0E899014C0463C12BB.asc
BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  gnupg2
Requires:       python3-setuptools
Requires:       python3-%{libname} = %{?epoch:%{epoch}:}%{version}-%{release}

%description
Nmstate is a library with an accompanying command line tool that manages host
networking settings in a declarative manner and aimed to satisfy enterprise
needs to manage host networking through a northbound declarative API and multi
provider support on the southbound.


%package -n python3-%{libname}
Summary:        nmstate Python 3 API library
Requires:       NetworkManager-libnm >= 1:1.22.8
# Use Recommends for NetworkManager because only access to NM DBus is required,
# but NM could be running on a different host
Recommends:     NetworkManager
# Avoid automatically generated profiles
Recommends:     NetworkManager-config-server
# Use Suggests for NetworkManager-ovs and NetworkManager-team since it is only
# required for OVS and team support
Suggests:       NetworkManager-ovs
Suggests:       NetworkManager-team


%description -n python3-%{libname}
This package contains the Python 3 library for Nmstate.

%prep
gpg2 --import --import-options import-export,import-minimal %{SOURCE2} > ./gpgkey-F7910D93CA83D77348595C0E899014C0463C12BB.gpg
gpgv2 --keyring ./gpgkey-F7910D93CA83D77348595C0E899014C0463C12BB.gpg %{SOURCE1} %{SOURCE0}
%setup -q

%build
%py3_build

%install
%py3_install

%files
%doc README.md
%doc examples/
%{_mandir}/man8/nmstatectl.8*
%{python3_sitelib}/nmstatectl
%{_bindir}/nmstatectl

%files -n python3-%{libname}
%license LICENSE
%{python3_sitelib}/%{libname}
%{python3_sitelib}/%{srcname}-*.egg-info/

%changelog
* Fri Jul 2 2021 kymayl<mayilin@kylinos.cn> - 0.2.7-1
- Package init with version 0.2.7-1
